const mongoose = require('mongoose')

const { ObjectId } = mongoose.Schema

const postSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  body: {
    type: String,
    required: true
  },
  photo: {
    data: Buffer,
    contenType: String
  },
  postedBy: {
    type: ObjectId,
    ref: 'User'
  }
}, { timestamps: true })

mongoose.model('Post', postSchema)
