const mongoose = require('mongoose')
require('../models/Post.js')

const Post = mongoose.model('Post')
const formidable = require('formidable')
const fs = require('fs')
const _ = require('lodash')

exports.postById = (req, res, next, id) => {
  Post.findById(id)
    .populate('postedBy', '_id name')
    .select('_id title body createdAt updatedAt photo')
    .exec((err, post) => {
      if (err || !post) {
        return res.status(400).json({
          error: err
        })
      }
      req.post = post
      next()
    })
}

exports.getPosts = (req, res) => {
  Post.find()
    .populate('postedBy', '_id name')
    .select('_id title body createdAt updatedAt')
    .sort({ updatedAt: -1 })
    .then((posts) => {
      res.status(200).json(posts)
    })
    .catch(err => console.log(err))
}

exports.createPost = (req, res, next) => {
  const form = new formidable.IncomingForm()
  form.keepExtensions = true
  form.parse(req, (err, fields, files) => {
    if (err) {
      return res.status(400).json({
        error: 'Image could not be uploaded'
      })
    }
    const post = new Post(fields)

    req.profile.hashed_password = undefined
    req.profile.salt = undefined
    post.postedBy = req.profile

    if (files.photo) {
      post.photo.data = fs.readFileSync(files.photo.path)
      post.photo.contentType = files.photo.type
    }
    post.save((err, result) => {
      if (err) {
        return res.status(400).json({
          error: err
        })
      }
      res.json(result)
    })
  })
}

exports.postsByUser = (req, res) => {
  Post.find({ postedBy: req.profile._id })
    .populate('postedBy', '_id name')
    .select('_id title body createdAt updatedAt')
    .sort('updatedUt')
    .exec((err, posts) => {
      if (err) {
        return res.status(400).json({
          error: err
        })
      }
      res.json(posts)
    })
}

exports.freePostsByUser = (req, res) => {
  Post.find({ postedBy: req.profile._id })
    .populate('postedBy', '_id name')
    .select('_id title body createdAt updatedAt')
    .sort('updatedAt')
    .exec((err, posts) => {
      if (err) {
        return res.status(400).json({
          error: err
        })
      }
      res.json(posts)
    })
}

exports.isPoster = (req, res, next) => {
  const sameUser = req.post && req.auth && req.post.postedBy._id.toString() === req.auth._id.toString()
  const adminUser = req.post && req.auth && req.auth.role === 'admin'
  const isPoster = sameUser || adminUser

  if (!isPoster) {
    return res.status(403).json({
      error: 'User is not authorized'
    })
  }
  next()
}

exports.deletePost = (req, res) => {
  const { post } = req
  post.remove((err, post) => {
    if (err) {
      return res.status(400).json({
        error: err
      })
    }
    res.json({
      message: 'Post deleted successfully'
    })
  })
}

exports.updatePost = (req, res, next) => {
  const form = new formidable.IncomingForm()
  form.keepExtensions = true
  form.parse(req, (err, fields, files) => {
    if (err) {
      return res.status(400).json({
        error: 'Photo could not be uploaded'
      })
    }
    let { post } = req
    post = _.extend(post, fields)

    if (files.photo) {
      post.photo.data = fs.readFileSync(files.photo.path)
      post.photo.contentType = files.photo.type
    }

    post.save((err, result) => {
      if (err) {
        return res.status(400).json({
          error: err
        })
      }
      res.json(post)
    })
  })
}

exports.photo = (req, res, next) => {
  if (req.post.photo.data) {
    res.set('Content-Type', req.post.photo.contentType)
    return res.send(req.post.photo.data)
  }
  next()
}

exports.singlePost = (req, res) => res.json(req.post)
