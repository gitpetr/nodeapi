const jwt = require('jsonwebtoken')
require('dotenv').config()
const expressJwt = require('express-jwt')
const mongoose = require('mongoose')
require('../models/User')

const User = mongoose.model('User')
const _ = require('lodash')
const { sendEmail } = require('../helpers')

exports.signup = async (req, res) => {
  const userExists = await User.findOne({ email: req.body.email })
  if (userExists) {
    return res.status(403).json({
      error: 'Такой email уже зарегистрирован!'
    })
  }
  const user = await new User(req.body)
  await user.save()
  res.status(200).json({ message: 'Регистрация прошла успешно! Please login.' })
}

exports.signin = (req, res) => {
  const { email, password } = req.body
  User.findOne({ email }, (err, user) => {
    if (err || !user) {
      return res.status(401).json({
        error: 'Такой email не зарегистрирован, Пожалуйста зарегистрируйтесь.'
      })
    }
    if (!user.authenticate(password)) {
      return res.status(401).json({
        error: 'Email и пароль не совпали'
      })
    }
    const token = jwt.sign(
      { _id: user._id, role: user.role },
      process.env.JWT_SECRET
    )
    res.cookie('t', token, { expire: new Date() + 9999 })
    const {
      _id, name, email, role
    } = user
    return res.json({
      token,
      user: {
        _id, email, name, role
      }
    })
  })
}

exports.signout = (req, res) => {
  res.clearCookie('t')
  return res.json({ message: 'Signout success!' })
}

exports.requireSignin = expressJwt({
  secret: process.env.JWT_SECRET,
  userProperty: 'auth'
})

exports.forgotPassword = (req, res) => {
  if (!req.body) return res.status(400).json({ message: 'Запрос пустой' })
  if (!req.body.email) { return res.status(400).json({ message: 'Введите, пожалуйста, Email' }) }

  const { email } = req.body

  User.findOne({ email }, (err, user) => {
    if (err || !user) {
      return res.status('401').json({
        error: 'Такой email не зарегистрирован!'
      })
    }

    const token = jwt.sign(
      { _id: user._id, iss: 'NODEAPI' },
      process.env.JWT_SECRET
    )

    const emailData = {
      from: 'noreply@node-react.com',
      to: email,
      subject: 'Инструкция по восстановлению пароля',
      text: `Чтобы сбросить старый пароль и установить новый, пожалуйста, перейдите по ссылке: ${
        process.env.CLIENT_URL
      }/reset-password/${token}`,
      html: `<p>Ссылка для восстановления пароля:</p> <p>${
        process.env.CLIENT_URL
      }/reset-password/${token}</p>`
    }

    return user.updateOne({ resetPasswordLink: token }, (err, success) => {
      if (err) {
        return res.json({ message: err })
      }
      sendEmail(emailData)
      return res.status(200).json({
        message: `По адресу ${email} было послано письмо. Для сброса пароля следуйте инструкциям в этом письме.`
      })
    })
  })
}

exports.resetPassword = (req, res) => {
  const { resetPasswordLink, newPassword } = req.body

  User.findOne({ resetPasswordLink }, (err, user) => {
    if (err || !user) {
      return res.status('401').json({
        error: 'Invalid Link!'
      })
    }

    const updatedFields = {
      password: newPassword,
      resetPasswordLink: ''
    }

    user = _.extend(user, updatedFields)

    user.save((err, result) => {
      if (err) {
        return res.status(400).json({
          error: err
        })
      }
      res.json({
        message: 'Великолепно! Теперь вы можете войти с новым паролем.'
      })
    })
  })
}
