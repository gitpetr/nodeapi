const express = require('express')
const {
  getPosts,
  createPost,
  postsByUser,
  freePostsByUser,
  postById,
  isPoster,
  updatePost,
  deletePost,
  photo,
  singlePost
} = require('../controllers/post')

const { createPostValidator } = require('../validator')
const { requireSignin } = require('../controllers/auth')
const { userById } = require('../controllers/user')

const router = express.Router()

router.get('/posts', getPosts)
router.post('/post/new/:userId', requireSignin, createPost, createPostValidator)
router.get('/posts/by/:userId', requireSignin, postsByUser)
router.get('/posts/free/by/:userId', freePostsByUser)
router.get('/post/:postId', singlePost)
router.put('/post/:postId', requireSignin, isPoster, updatePost)
router.delete('/post/:postId', requireSignin, isPoster, deletePost)

router.get('/post/photo/:postId', photo)

router.param('userId', userById)
router.param('postId', postById)

module.exports = router
